<?php
$selectValue    = (isset($_POST["selectValue"]) && !empty($_POST["selectValue"])) ? $_POST["selectValue"] : "Undefined";
$switchActive   = (isset($_POST["switchActive"]) && !empty($_POST["switchActive"])) ? $_POST["switchActive"] : false;
$checkboxActive = (isset($_POST["checkboxActive"]) && !empty($_POST["checkboxActive"])) ? $_POST["checkboxActive"] : false;
$radioValue     = (isset($_POST["radioValue"]) && !empty($_POST["radioValue"])) ? $_POST["radioValue"] : "Undefined";

$response = "Response from server" .
"\n==================" .
"\nSelect Value:\t\t" . $selectValue .
"\nSwitch Active:\t\t" . $switchActive .
"\nCheckbox Active:\t" . $checkboxActive .
"\nRadio Value:\t\t\t" . $radioValue;

echo $response;
?>

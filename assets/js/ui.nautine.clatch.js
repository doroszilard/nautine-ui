/**
 * Nautine Clatch UI Scripts
 *
 * Source bundle: Clatch Nautine
 * Developer:     Szilard Doro
 * Date:          2016-08-24
 *
 * Version: 1.0
 */

/**
 * This function checks if specific element is active or not.
 *
 * @returns {*}
 */
$.fn.isActive = function() {
    var $e = $(this);

    if (!$e.hasClass('switch__nautine') && !$e.hasClass('checkbox__nautine') && !$e.hasClass('radio-button__nautine')) {
        console.log('[Error] #' + $e.attr('id') + ' is not a Nautine Switch or Nautine Checkbox.');

        return undefined;
    }

    return $e.hasClass('active');
};

/**
 * This function returns selected value of a select input.
 *
 * @returns {*}
 */
$.fn.getSelectedOption = function() {
    var $e = $(this);

    // Return undefined if element is not a Nautine Select.
    if (!$e.hasClass('select__nautine')) {
        console.log('[Error] #' + $e.attr('id') + ' is not a Nautine Select.');

        return undefined;
    }

    return $e.find('div.option.selected').find('span').data('value');
};

/**
 * This function returns selected value of a radio group.
 *
 * @returns {*}
 */
$.fn.getSelectedRadio = function() {
    var $e = $(this);

    if (!$e.hasClass('radio__nautine')) {
        console.log('[Error] #' + $e.attr('id') + ' is not a Nautine Radio.');

        return undefined;
    }

    return $e.find('div.radio-button__nautine.active').data('value');
};

// Nautine UI Library
var nautineUI = (function() {

    // Declare main Nautine UI elements in this array.
    $elements = {
        nautineSelect:   $('div.select__nautine'),
        nautineSwitch:   $('div.switch__nautine'),
        nautineCheckbox: $('div.checkbox__nautine'),
        nautineRadio:    $('div.radio__nautine')
    }

    /**
     * Set selected option with this function.
     */
    $.fn.setSelectedOption = function(value) {
        $e = $(this);

        var $selectOptions = $e.find('div.option');

        // If no value is specified then set first radio button
        if (!value) {
            $selectOptions.removeClass('selected').hide();
            $selectOptions.first().addClass('selected').show();
        } else {
            $.each($selectOptions, function(i, elem) {
                $selectOption = $(elem);

                if ($selectOption.find('span').data('value') === value) {
                    $selectOptions.removeClass('selected').hide();
                    $selectOption.addClass('selected').show();

                    return true;
                }
            });
        }
    }

    /**
     * Set active status of switch or checkbox with this function.
     */
    $.fn.setActive = function(value) {
        $e = $(this);

        if ($e.hasClass('switch__nautine')) {
            // Check if any value is specified
            if (!value || (value === false || $e.data('default') === false)) {
                if ($e.isActive()) {
                    // Animate switch
                    _animateSwitch($e, 250);
                }
            } else {
                if (!$e.isActive()) {
                    // Animate switch
                    _animateSwitch($e, 250);
                }
            }
        }

        if ($e.hasClass('checkbox__nautine')) {
            // Check if any value is specified
            if (!value || (value === false || $e.data('default') === false)) {
                if ($e.isActive()) {
                    // Active checkbox
                    $e.removeClass('active');
                }
            } else {
                if (!$e.isActive()) {
                    // Deactive checkbox
                    $e.addClass('active');
                }
            }
        }
    };

    /**
     * Set selected radio button with this function.
     */
    $.fn.setSelectedRadio = function(value) {
        $e = $(this);

        var $radioButtons = $e.find('div.radio-button__nautine');

        // If no value is specified then set first radio button
        if (!value) {
            $radioButtons.removeClass('active');
            $radioButtons.first().addClass('active');
        } else {
            $.each($radioButtons, function(i, elem) {
                $radioButton = $(elem);

                if ($radioButton.data('value') === value) {
                    $radioButtons.removeClass('active');
                    $radioButton.addClass('active');

                    return true;
                }
            });
        }
    };

    /**
     * This function resets every Nautine UI elements to their default values.
     */
    function _resetInputs($parent) {
        if (!$parent) {
            console.log('[Error] Please specify a parent for Nautine UI elements.');

            return false;
        }

        // Nautine UI elements
        $nautineSelect   = $parent.find('div.select__nautine');
        $nautineSwitch   = $parent.find('div.switch__nautine');
        $nautineCheckbox = $parent.find('div.checkbox__nautine');
        $nautineRadio    = $parent.find('div.radio__nautine');

        // Reset Nautine Select if exists
        if ($nautineSelect.length) {
            // Iterate through Nautine Selects
            $.each($nautineSelect, function(i, e) {
                var $nSelect = $(e);
                var defaultValue = $nSelect.data('default');

                // Set default option
                $nSelect.setSelectedOption(defaultValue);
            });
        }

        // Reset Nautine Switch if exists
        if ($nautineSwitch.length) {
            $.each($nautineSwitch, function(i, e) {
                var $nSwitch = $(e);
                var defaultValue = $nSwitch.data('default');

                $nSwitch.setActive(defaultValue);
            });
        }

        // Reset Nautine Checkbox if exists
        if ($nautineCheckbox.length) {
            $.each($nautineCheckbox, function(i, e) {
                var $nCheckbox = $(e);
                var defaultValue = $nCheckbox.data('default');

                $nCheckbox.setActive(defaultValue);
            });
        }

        // Reset Nautine Radio if exists
        if ($nautineRadio.length) {
            $.each($nautineRadio, function(i, e) {
                var $nRadio = $(e);
                var defaultValue = $nRadio.data('default');

                $nRadio.setSelectedRadio(defaultValue);
            });
        }
    }

    /**
     * This function binds select click event to select inputs.
     */
    function _setupNautineSelect() {
        // Return if no selects are present
        if (!$elements.nautineSelect.length) {
            return;
        }

        /**
         * Animate option box with this event.
         */
        $('div.select__nautine, div.select__toggle__nautine').on('click', function(evt) {
            evt.preventDefault();

            // Show or hide options
            showHideDefaultOptions($(this));
        });

        /**
         * Handle option box click with this event.
         */
        $('div.option').on('click', function(evt) {
            var $clickedOption = $(this);
            var $allOptions    = $(this).parent().find('div.option');

            var _CLASS = 'selected';

            if (!$clickedOption.hasClass(_CLASS)) {
                $allOptions.removeClass(_CLASS);
                $clickedOption.addClass(_CLASS);
            }
        });

        /**
         * Use this function to show or hide not selected options.
         */
        function showHideDefaultOptions($e) {
            var $nautineSelect  = $e.parent().find($elements.nautineSelect);
            var $defaultOptions = $nautineSelect
                .find('div.option')
                .not($('div.option.selected'));

            if ($defaultOptions.is(':visible')) {
                // Overlay issue fix
                $nautineSelect.css({
                    zIndex: ($nautineSelect.css('z-index') - 1)
                });

                // Hide not selected options
                $defaultOptions.hide();
            } else {
                // Overlay issue fix
                $nautineSelect.css({
                    zIndex: ($nautineSelect.css('z-index') + 1)
                });

                // Show not selected options
                $defaultOptions.show();
            }
        }
    }

    /**
     * This function binds switch click event to switches.
     *
     * @param animationTimer {integer} Time of animation completion
     */
    function _setupNautineSwitch(animationTimer) {
        // Return if no switches are present
        if (!$elements.nautineSwitch.length) {
            return;
        }

        $elements.nautineSwitch.on('click', function() {
            var $switchNautine = $(this);

            // Animate Nautine Switch
            _animateSwitch($switchNautine, animationTimer);

            // Instantiate custom event that will be dispatched
            var _switchChangeEvent = new CustomEvent('nautineSwitchChanged', {
                'detail': $switchNautine.attr('id')
            });

            // Dispatch event to window
            window.dispatchEvent(_switchChangeEvent);
        });
    }

    /**
     * This function binds checkbox click event to sliders.
     */
    function _setupNautineCheckbox() {
        // Return if no checkboxes are present
        if (!$elements.nautineCheckbox.length) {
            return;
        }

        $elements.nautineCheckbox.on('click', function() {
            var $checkboxNautine = $(this);
            var $checkboxPipe    = $(this).find('div.checkbox-pipe');

            $checkboxNautine.toggleClass('active');

            // Instantiate custom event that will be dispatched
            var _checkboxChangeEvent = new CustomEvent('nautineCheckboxChanged', {
                'detail': $checkboxNautine.attr('id')
            });

            // Dispatch event to window
            window.dispatchEvent(_checkboxChangeEvent);
        });
    }

    /**
     * This function binds radio button click event to radio buttons.
     */
    function _setupNautineRadio() {
        // Return if no radio buttons are present
        if (!$elements.nautineRadio.length) {
            return;
        }

        var $radioButtons = $elements.nautineRadio.find('div.radio-button__nautine');

        $radioButtons.on('click', function() {
            var $radioButtonNautine = $(this);

            if (!$radioButtonNautine.isActive()) {
                $radioButtons.removeClass('active');
                $radioButtonNautine.addClass('active');
            }
        });
    }

    /**
     * Slide animate Nautine UI switch with this function.
     *
     * @param {nautineSwitch} nautineSwitch Nautine UI Switch
     * @param {Integer} animationTimer Animation timer
     */
    function _animateSwitch($nautineSwitch, animationTimer) {
        var $button = $nautineSwitch.find('div.button');

        var sliderWidth = parseInt($nautineSwitch.css('width')) - parseInt($button.css('width')) - parseInt($button.css('left'));

        // Check if slider is already active or not
        if ($nautineSwitch.isActive()) {
            $button.finish().animate({
                marginLeft: 0
            }, animationTimer, function() {
                // Toggle activity indicator class on slider
                $nautineSwitch.toggleClass('active');
            });
        } else {
            $button.finish().animate({
                marginLeft: '+=' + sliderWidth
            }, animationTimer, function() {
                // Toggle activity indicator class on slider
                $nautineSwitch.toggleClass('active');
            });
        }
    }

    // Public API
    return {
        initNautineSelect:   _setupNautineSelect,
        initNautineSwitch:   _setupNautineSwitch,
        initNautineCheckbox: _setupNautineCheckbox,
        initNautineRadio:    _setupNautineRadio,
        reset:               _resetInputs
    }
})();
